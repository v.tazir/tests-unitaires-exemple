#include "arithmetique.h"
#include <exception>

long arithmetique::addition(int operandeA, int operandeB)
{
  return static_cast<long int>(operandeA) + operandeB;
}

long arithmetique::soustraction(int operandeA, int operandeB)
{
  return (long)operandeA - operandeB;
}

long arithmetique::multiplication(int operandeA, int operandeB)
{
  return (long)operandeA * operandeB;
}

long arithmetique::division(int dividende, int diviseur)
{
  if(diviseur == 0){
    throw std::exception();
  }
  return (long)dividende / diviseur;
}

#ifndef ARITHMETIQUE_H
#define ARITHMETIQUE_H

namespace arithmetique {
  long addition(int, int);
  long soustraction(int, int);
  long multiplication(int, int);
  long division(int, int);
}

#endif // ARITHMETIQUE_H

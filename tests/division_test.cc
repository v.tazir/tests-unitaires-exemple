#include "division_test.h"
#include "arithmetique.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <limits>

// Enregistrer la classe de test dans le registre de la suite
CPPUNIT_TEST_SUITE_REGISTRATION(divisionTest);

void divisionTest::setUp() {}

void divisionTest::tearDown() {}

/* TODO: Ajouter les définitions des méthodes de test de la classe
 * divisionTest
 */
void divisionTest::division_normale()
{
  dividende = 6;
  diviseur = 2;
  CPPUNIT_ASSERT_EQUAL((long)3,
                       arithmetique::division(dividende, diviseur)
    );
  diviseur = -2;
  CPPUNIT_ASSERT_EQUAL((long)-3,
                       arithmetique::division(dividende, diviseur)
    );
}

void divisionTest::division_par_zero()
{
  dividende = 123;
  diviseur = 0;
  CPPUNIT_ASSERT_THROW(arithmetique::division(dividende, diviseur),
                       std::exception
    );
}

void divisionTest::division_min()
{
  dividende = std::numeric_limits<int>::lowest();
  diviseur = -1;
  CPPUNIT_ASSERT_EQUAL(-(long)dividende,
                       (long)arithmetique::division(dividende, diviseur)
    );
}

void divisionTest::division_max()
{
  dividende = std::numeric_limits<int>::max();
  diviseur = 1;
  CPPUNIT_ASSERT_EQUAL((long)dividende,
                       arithmetique::division(dividende, diviseur) 
    );
}

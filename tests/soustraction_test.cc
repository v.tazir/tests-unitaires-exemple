#include "soustraction_test.h"
#include "arithmetique.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <limits>

// Enregistrer la classe de test dans le registre de la suite
CPPUNIT_TEST_SUITE_REGISTRATION(soustractionTest);

void soustractionTest::setUp() {}

void soustractionTest::tearDown() {}

/* TODO: Ajouter les définitions des méthodes de test de la classe
 * soustractionTest
 */
void soustractionTest::soustraction_normale()
{
  opA = 2;
  opB = 1;
  CPPUNIT_ASSERT_EQUAL((long)1,
                       arithmetique::soustraction(opA, opB)
    );
  opB = 3;
  CPPUNIT_ASSERT_EQUAL((long)-1,
                       arithmetique::soustraction(opA, opB)
    );
}

void soustractionTest::soustraction_max()
{
  opA = std::numeric_limits<int>::lowest();
  opB = 1;
  CPPUNIT_ASSERT_LESS((long)opA,
                      arithmetique::soustraction(opA, opB)
    );
}

void soustractionTest::soustraction_zero()
{
  opA = 1;
  opB = 1;
  CPPUNIT_ASSERT_EQUAL((long)0,
                       arithmetique::soustraction(opA, opB)
    );  
}

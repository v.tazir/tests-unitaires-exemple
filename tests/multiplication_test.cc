#include "multiplication_test.h"
#include "arithmetique.h"
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <limits>

// Enregistrer la classe de test dans le registre de la suite
CPPUNIT_TEST_SUITE_REGISTRATION(multiplicationTest);

void multiplicationTest::setUp() {}

void multiplicationTest::tearDown() {}

/* TODO: Ajouter les définitions des méthodes de test de la classe
 * multiplicationTest
 */
void multiplicationTest::multiplication_normale()
{
  opA = 2;
  opB = 3;
  CPPUNIT_ASSERT_EQUAL((long)6,
                       arithmetique::multiplication(opA, opB)
    );
  
  opB = -3;
  CPPUNIT_ASSERT_EQUAL((long)-6,
                       arithmetique::multiplication(opA, opB)
    );
}

void multiplicationTest::multiplication_max()
{
  opA = std::numeric_limits<int>::max();
  opB = opA;
  CPPUNIT_ASSERT_GREATER((long)opA,
                         arithmetique::multiplication(opA, opB)
    );
}

void multiplicationTest::multiplication_min()
{
  opA = std::numeric_limits<int>::max();
  opB = std::numeric_limits<int>::lowest();
  CPPUNIT_ASSERT_LESS((long)opB,
                      arithmetique::multiplication(opA, opB)
    );
}

void multiplicationTest::multiplication_zero()
{
  opA = 0;
  opB = opA;
  CPPUNIT_ASSERT_EQUAL((long)0,
                      arithmetique::multiplication(opA, opB)
    );

  opB = 300;
  CPPUNIT_ASSERT_EQUAL((long)0,
                      arithmetique::multiplication(opA, opB)
    );
}
